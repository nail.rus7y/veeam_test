### Ответы на вопросы по JavaScript

1. Что выведет консоль? Аргументируйте свой ответ
```javascript
(function() {
    f();

    f = function() {
        console.log(1);
    }
})()

function f() {
    console.log(2)
}

f();
```
**Ответ**: Консоль выведет `2`, затем `1` т.к. функция `f` является *function declaration*, поэтому из за механизма
поднятия она определится вверху нашей области видимости. *IIFE* в свою очередь является *function expression*, поэтому
она будет объявлена на своем месте. После вызова *IIFE* сначала вызывается `f` и выводится в консоль `2`, затем
функция переопределяется новой и позже снова будет вызвана и выведет `1`.

2. Что выведет консоль? Аргументируйте свой ответ
```javascript
const obj = {
  name: 'John',
  getName() {
    return this.name;
  }
};

const name1 = obj.getName();
const getName = obj.getName;
const name2 = getName();
console.log(`${name1} ${name2}`); // ?
```
**Ответ**: `John` и `undefined`. `this` метода `getName` определяется в момент вызова. В первом случае
вызов происходит с объектом `obj`, во втором случае мы присваиваем функцию в переменную, которая имеет
контекст окружения (вероятно `window`)

3. Создайте метод у объекта String, который многократно повторяет строку (не используя метод .repeat()). 
```javascript
String.prototype.customRepeat = function(n) {
    let result = "";
    for (let i = 0; i < n; i++) {
        result += this;
    }
    
    return result;
}
```

4. Есть список элементов button, на которые навешен обработчик события ‘click’
   Что попадет в консоль, если пользователь нажмет первую и последнюю кнопку в списке? Аргументируйте свой ответ.
```javascript
    var nodes = document.getElementsByTagName('button');
    for (var i = 0; i < nodes.length; i++) {
        nodes[i].addEventListener('click', function() {
            console.log('You clicked element #' + i);
        });
    }
```   
**Ответ**: В консоль дважды выведется `You clicked element # (nodes.length)`. Это происходит из за механизма поднятия для переменных объявленных через `var`, 
она объявится как бы над нашим циклом, поэтому все вызовы клика будут ссылаться на одну и ту же переменную, которая в конце цикла будет равна числу кнопок.

5. Что выведет в консоль следующий код? Аргументируйте свой ответ.
```javascript
function printme() {
    console.log(1);
    setTimeout(function() { console.log(2); }, 1000);
    setTimeout(function() { console.log(3); }, 0);
    console.log(4);
}

printme();
```
**Ответ**: В консоль выведется `1, 4, 3, 2`. Асинхронная операция `setTimeout` является частью
*WebApi* и обрабатывается с помощью механизма *event loop*. Как только пройдет время задержки в таймере, 
механизм поместит функции обратного вызова в очередь. Функции в этой очереди буду помещаться им в основной стек только в 
момент когда стек вызова будет пуст, поэтому таймер с нулевой задержкой будет вызван позже последнего лога.

6. Напишите функцию isPalindrome(str), которая проверяет, является ли строка палиндромом
```javascript
function isPalidrome(str) {
    // Используем регулярку на случай если строка имеет посторонние символы
    str = str.toLowerCase().replace(/[^a-z]/gi, "");
    
    for (let i = 0; i < Math.floor(str.length / 2); i++) {
        if (str[i] !== str[str.length - i - 1]) return false;
    }
    
    return true;
}
```

7. Напишите функцию, складывающую 2 числа, которую можно вызывать следующим образом:
```javascript
function sum(...args) {
    if (args.length < 2) {
        let a = args[0];
        return (b) => a + b;
    } else {
        return args[0] + args[1];
    }
}
```

8. Поменяйте местами значения целочисленных переменных, не используя временные переменные. Первый вариант - используя ES6, и второй вариант – используя ES5.
```javascript
var a = 1;
var b = 2;

ES6
[b, a] = [a, b];

ES5

b = a + b;
a = b - a;
b = b - a;
```

9. Напишите функцию, проверяющую число на четность, используя только битовые операции
```javascript
function isEven(num) {
    return !(num & 1);
}
```