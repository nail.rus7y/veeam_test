import React, {ChangeEvent, useEffect, useState} from 'react';
import {Nullable} from "./@types/global";
import {UploadedImage} from "./modules/UploadedImage";
import {ImagePreview} from "./modules/ImagePreview";
import {ImageControls} from "./modules/ImageControls";
import styles from "./App.module.css";

function App() {
  const [file, setFile] = useState<Nullable<File>>(null);
  const [error, setError] = useState<string>("");
  const [image, setImage] = useState<string>("");
  const [previewActive, setPreviewActive] = useState(false);

  const handleUploadFile = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setFile(e.target.files[0]);
      setError("");
    } else {
      setError("Choose a file");
    }
  }

  const handleOpenPreview = () => {
    setPreviewActive(true);
  }

  const handleClosePreview = () => {
    setPreviewActive(false);
  }

  useEffect(() => {
    let blob = "";
    if (file) {
      blob = URL.createObjectURL(file);
      setImage(blob);
      setError("");
    }
    return () => URL.revokeObjectURL(blob);
  }, [file])

  return (
    <>
      <div className={styles.app}>
        <ImageControls error={error} image={image} handleOpenPreview={handleOpenPreview} handleUploadFile={handleUploadFile} />
        <UploadedImage image={image} onOpenPreview={handleOpenPreview} />
      </div>
      {previewActive && <ImagePreview image={image} handleClosePreview={handleClosePreview} />}
    </>
  );
}

export default App;
