import {UploadError} from "./UploadError";
import React, {ChangeEvent} from "react";
import styles from "./ImageControls.module.css";

interface ImageActionControlsProps {
  handleOpenPreview: () => void;
  handleUploadFile: (e: ChangeEvent<HTMLInputElement>) => void;
  error: string;
  image: string;
}

export default function ({ error, image, handleUploadFile, handleOpenPreview }: ImageActionControlsProps) {
  return (
    <div className={styles.imageControlsWrapper}>
      <div className={styles.controls}>
        <input id={"uploadImage"} className={styles.btn} onChange={handleUploadFile} type={"file"} accept={"image/*"} />
        <button className={styles.btn} onClick={handleOpenPreview} disabled={!image}>Image preview</button>
      </div>
      <UploadError error={error} />
    </div>
  )
}