import React from "react";
import styles from "./UploadError.module.css";

interface UploadErrorProps {
  error: string;
}

export default function ({ error }: UploadErrorProps) {
  return (
      error ? (
        <div className={styles.error}>{error}</div>
      ) : null
  )
}