import styles from "../ImagePreview.module.css";
import React from "react";

interface ClosePreviewProps {
  handleClosePreview: () => void;
}

export default function ({ handleClosePreview }: ClosePreviewProps) {
  return (
    <button className={styles.closeBtn} onClick={handleClosePreview}>Close</button>

  )
}