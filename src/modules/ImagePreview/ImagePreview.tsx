import React, {MouseEventHandler, useEffect, useRef, useState} from "react";
import {Nullable} from "../../@types/global";
import {Mark} from "./Mark";
import styles from "./ImagePreview.module.css";
import { v4 } from "uuid";
import {ClosePreview} from "./ClosePreview";

interface ImagePreviewProps {
  image: string;
  handleClosePreview: () => void;
}

export default function ({ handleClosePreview, image }: ImagePreviewProps) {
  const imageRef = useRef<Nullable<HTMLImageElement>>(null);
  const wrapperRef = useRef<Nullable<HTMLDivElement>>(null);
  const [imgSize, setImageSize] = useState<Record<"width" | "height", number>>({ width: 0, height: 0 });
  const [marks, setMarks] = useState<{ x: number, y: number }[]>([])
  const [isHorizontalScale, setIsHorizontalScale] = useState(false);

  const handleCreateMark: MouseEventHandler<HTMLImageElement> = (e) => {
    const { pageX, pageY } = e;
    setMarks(prevState => [...prevState, { x: pageX - wrapperRef.current!.offsetLeft, y: pageY - wrapperRef.current!.offsetTop }])
  }

  const handleResize = () => {
    setMarks(prevState => prevState.map(({ x, y }) => {
      const newX = x * (imageRef.current!.width / imgSize.width);
      const newY = y * (imageRef.current!.height / imgSize.height);
      return { y: newY, x: newX}
    }))
    setImageSize({ height: imageRef.current!.height, width: imageRef.current!.width });
  }

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, [imgSize.width, imgSize.height])

  useEffect(() => {
    if (imageRef.current) {
      setIsHorizontalScale(imageRef.current.naturalWidth > imageRef.current.naturalHeight )
      setImageSize({ width: imageRef.current!.width, height: imageRef.current!.height })
    }
  }, [imageRef.current]);


  return (
    <div className={styles.imagePreviewWrapper}>
      <div ref={wrapperRef} className={styles.imageWrapper} style={{ ...(isHorizontalScale ? { maxHeight: "100%", } : { height: "100%" }) }}>
        <ClosePreview handleClosePreview={handleClosePreview} />
        <img ref={imageRef} onClick={handleCreateMark} style={{ display: "block", ...(isHorizontalScale ? { width: "100%", } : { height: "100%" }) }} alt={"Preview"} src={image} />
        {marks.map(({ x, y }) => {
          return <Mark key={v4()} x={x} y={y} />
        })}
      </div>
    </div>
  )
}