import React from "react";
import styles from "./Mark.module.css";

interface MarkProps {
  x: number;
  y: number;
}

export default function ({ y, x }: MarkProps) {
  return (
    <div className={styles.mark} style={{ top: y.toString() + "px", left: x.toString() + "px" }}>
      This is a mark
    </div>
  )
}