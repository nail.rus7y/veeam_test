import React from "react";
import styles from "./UploadedImage.module.css";

interface UploadedImageProps {
  image: string;
  onOpenPreview: () => void;
}

export default function({ image, onOpenPreview }: UploadedImageProps) {
  return (
      <div className={styles.imageWrapper}>
        {image
          ? (
            <img className={styles.image} onClick={onOpenPreview}  src={image} alt={"Preview"} />
          )
          : null
        }
      </div>
  );
}